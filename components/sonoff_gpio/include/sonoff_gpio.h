#include "esp_system.h"
#include "esp_err.h"

#include "driver/gpio.h"

#define RELAY_PIN GPIO_NUM_12
#define POWER_LED_PIN GPIO_NUM_13
#define INDICATOR_LED_PIN GPIO_NUM_14

#define SONOFF_POWER_LED_SET_LEVEL(level) gpio_set_level(POWER_LED_PIN, level)
#define SONOFF_RELAY_SET_LEVEL(level) gpio_set_level(RELAY_PIN, level)

esp_err_t sonoff_gpio_init(void);
