#include "sonoff_gpio.h"

esp_err_t sonoff_gpio_init(void)
{
    gpio_config_t neopixel_config;
    neopixel_config.pin_bit_mask = 1 << INDICATOR_LED_PIN;
    neopixel_config.mode = GPIO_MODE_OUTPUT;
    neopixel_config.pull_down_en = GPIO_PULLDOWN_ENABLE;
    neopixel_config.pull_up_en = GPIO_PULLUP_DISABLE;
    neopixel_config.intr_type = GPIO_INTR_DISABLE;
    ESP_ERROR_CHECK(gpio_config(&neopixel_config));

    gpio_config_t led_config;
    led_config.pin_bit_mask = 1 << POWER_LED_PIN;
    led_config.mode = GPIO_MODE_OUTPUT;
    led_config.pull_down_en = GPIO_PULLDOWN_DISABLE;
    led_config.pull_up_en = GPIO_PULLUP_ENABLE;
    led_config.intr_type = GPIO_INTR_DISABLE;
    ESP_ERROR_CHECK(gpio_config(&led_config));

    gpio_config_t relay_config;
    relay_config.pin_bit_mask = 1 << RELAY_PIN;
    relay_config.mode = GPIO_MODE_OUTPUT;
    relay_config.pull_down_en = GPIO_PULLDOWN_DISABLE;
    relay_config.pull_up_en = GPIO_PULLUP_ENABLE;
    relay_config.intr_type = GPIO_INTR_DISABLE;
    ESP_ERROR_CHECK(gpio_config(&relay_config));

    return ESP_OK;
}