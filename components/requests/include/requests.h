#include <stdio.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_event_loop.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "esp_tls.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"

#define WEB_SERVER "api.thecore.sdu.dk"
#define WEB_PORT 443
#define WEB_URL "https://api.thecore.sdu.dk"

#define HTTP_TYPE "HTTP/1.0\r\n"
#define USER_AGENT "User-Agent: esp-idf/3.1 esp8266\r\n"
#define CONTENT_TYPE "Content-Type: application/json\r\n"

#define BUF_SIZE 1024

typedef struct post_request_t
{
    char path[128];
    char data[128];
} post_request_t;

typedef struct session_info_t
{
    bool authorized;
    bool identify;
} session_info_t;

void get_request(char *, char *);

void post_request(char *, char *, char *);

void patch_request(char *, char *, char *, char *);

void get_http_code(char *, char *);

void get_id(char *, char *);

void get_token(char *, char *);

void get_session(char *, session_info_t *);

bool get_machine_id(char *, char *);

void set_status(char *, char *, bool, char *);