#include "requests.h"

#define ELEMENT_COUNT(x) (sizeof(x) / sizeof(x[0]))

static const char *TAG = "Simple node request";

extern const uint8_t server_root_cert_pem_start[] asm("_binary_server_root_cert_pem_start");
extern const uint8_t server_root_cert_pem_end[] asm("_binary_server_root_cert_pem_end");

void get_request(char *path, char *response)
{
    int ret;
    //assembling the request
    char request[256];
    snprintf(request,
             sizeof(request),
             "%s%s%s%s%s%s%s%s%s%s",
             "GET ",
             WEB_URL,
             path,
             " ",
             HTTP_TYPE,
             "Host: ",
             WEB_SERVER,
             "\r\n",
             USER_AGENT,
             "\r\n");

    ESP_LOGI(TAG, request);
    // ---------------- Example code from Espressif

    esp_tls_cfg_t cfg = {
        .cacert_pem_buf = server_root_cert_pem_start,
        .cacert_pem_bytes = server_root_cert_pem_end - server_root_cert_pem_start,
    };

    struct esp_tls *tls;

    while (1)
    {
        tls = esp_tls_conn_new(WEB_SERVER, strlen(WEB_SERVER), WEB_PORT, &cfg);

        if (tls == NULL)
        {
            ESP_LOGE(TAG, "TLS Connection failed...");
            esp_tls_conn_delete(tls);
            continue;
        }
        ESP_LOGI(TAG, "TLS Connection established...");
        break;
    }

    size_t written_bytes = 0;
    do
    {
        ret = esp_tls_conn_write(tls,
                                 request + written_bytes,
                                 strlen(request) - written_bytes);
        if (ret >= 0)
        {
            ESP_LOGI(TAG, "%d bytes written", ret);
            written_bytes += ret;
        }
        else if (ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            ESP_LOGE(TAG, "esp_tls_conn_write  returned 0x%x", ret);
            esp_tls_conn_delete(tls);
        }
    } while (written_bytes < strlen(request));

    ESP_LOGI(TAG, "Reading HTTP response...");

    // ---------------- End Example code from Espressif
    // ---------------- Below code is modified from the example
    bzero(response, BUF_SIZE); // Zero the output buffer
    while (1)
    {
        ret = esp_tls_conn_read(tls, response, BUF_SIZE);
        if (ret == MBEDTLS_ERR_SSL_WANT_WRITE || ret == MBEDTLS_ERR_SSL_WANT_READ)
        {
            ESP_LOGE(TAG, "EMBEDTLS error");
            continue;
        }

        if (ret < 0)
        {
            ESP_LOGE(TAG, "esp_tls_conn_read  returned -0x%x", -ret);
            break;
        }

        if (ret == 0)
        {
            ESP_LOGI(TAG, "connection closed");
            break;
        }

        ESP_LOGI(TAG, "%d bytes read", ret);
        break;
    }
    esp_tls_conn_delete(tls);
}

void post_request(char *path, char *data, char *response)
{
    int ret;

    //determine length of data
    uint8_t len = (uint8_t)strlen(data);

    //assembling the request
    char request[256];
    if (len == 1) //no data
    {
        snprintf(request,
                 sizeof(request),
                 "%s%s%s%s%s%s%s%s%s%s",
                 "POST ",
                 WEB_URL,
                 path,
                 " ",
                 HTTP_TYPE,
                 "Host: ",
                 WEB_SERVER,
                 "\r\n",
                 USER_AGENT,
                 "\r\n");
    }
    else
    {
        snprintf(request,
                 sizeof(request),
                 "%s%s%s%s%s%s%s%s%s%s%s%d%s%s%s",
                 "POST ",
                 WEB_URL,
                 path,
                 " ",
                 HTTP_TYPE,
                 "Host: ",
                 WEB_SERVER,
                 "\r\n",
                 USER_AGENT,
                 CONTENT_TYPE,
                 "Content-Length: ",
                 len,
                 "\r\n\r\n",
                 data,
                 "\r\n\r\n");
    }

    ESP_LOGI(TAG, request);

    // ---------------- Example code from Espressif

    esp_tls_cfg_t cfg = {
        .cacert_pem_buf = server_root_cert_pem_start,
        .cacert_pem_bytes = server_root_cert_pem_end - server_root_cert_pem_start,
    };

    struct esp_tls *tls;

    while (1)
    {
        tls = esp_tls_conn_new(WEB_SERVER, strlen(WEB_SERVER), WEB_PORT, &cfg);

        if (tls == NULL)
        {
            ESP_LOGE(TAG, "TLS Connection failed...");
            esp_tls_conn_delete(tls);
            continue;
        }
        ESP_LOGI(TAG, "TLS Connection established...");
        break;
    }

    size_t written_bytes = 0;
    do
    {
        ret = esp_tls_conn_write(tls,
                                 request + written_bytes,
                                 strlen(request) - written_bytes);
        if (ret >= 0)
        {
            ESP_LOGI(TAG, "%d bytes written", ret);
            written_bytes += ret;
        }
        else if (ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            ESP_LOGE(TAG, "esp_tls_conn_write  returned 0x%x", ret);
            esp_tls_conn_delete(tls);
        }
    } while (written_bytes < strlen(request));

    ESP_LOGI(TAG, "Reading HTTP response...");

    // ---------------- End Example code from Espressif
    // ---------------- Below code is modified from the example
    bzero(response, BUF_SIZE); // Zero the output buffer
    while (1)
    {
        ret = esp_tls_conn_read(tls, response, BUF_SIZE);
        if (ret == MBEDTLS_ERR_SSL_WANT_WRITE || ret == MBEDTLS_ERR_SSL_WANT_READ)
            continue;

        if (ret < 0)
        {
            ESP_LOGE(TAG, "esp_tls_conn_read  returned -0x%x", -ret);
            break;
        }

        if (ret == 0)
        {
            ESP_LOGI(TAG, "connection closed");
            break;
        }

        ESP_LOGI(TAG, "%d bytes read", ret);
        break;
    }
    esp_tls_conn_delete(tls);
}

void patch_request(char *path, char *token, char *data, char *response)
{
    int ret;

    //determine length of data
    uint8_t len = (uint8_t)strlen(data);

    //assembling the request
    char request[256];
    if (len == 1) //no data
    {
        snprintf(request,
                 sizeof(request),
                 "%s%s%s%s%s%s%s%s%s%s",
                 "POST ",
                 WEB_URL,
                 path,
                 " ",
                 HTTP_TYPE,
                 "Host: ",
                 WEB_SERVER,
                 "\r\n",
                 USER_AGENT,
                 "\r\n");
    }
    else
    {
        snprintf(request,
                 sizeof(request),
                 "%s%s%s%s%s%s%s%s%s%s%s%s%s%d%s%s%s",
                 "POST ",
                 WEB_URL,
                 path,
                 " ",
                 HTTP_TYPE,
                 "Host: ",
                 WEB_SERVER,
                 "\r\n",
                 USER_AGENT,
                 "Authorization: JWT ",
                 token,
                 CONTENT_TYPE,
                 "Content-Length: ",
                 len,
                 "\r\n\r\n",
                 data,
                 "\r\n\r\n");
    }

    ESP_LOGI(TAG, request);

    // ---------------- Example code from Espressif

    esp_tls_cfg_t cfg = {
        .cacert_pem_buf = server_root_cert_pem_start,
        .cacert_pem_bytes = server_root_cert_pem_end - server_root_cert_pem_start,
    };

    struct esp_tls *tls;

    while (1)
    {
        tls = esp_tls_conn_new(WEB_SERVER, strlen(WEB_SERVER), WEB_PORT, &cfg);

        if (tls == NULL)
        {
            ESP_LOGE(TAG, "TLS Connection failed...");
            esp_tls_conn_delete(tls);
            continue;
        }
        ESP_LOGI(TAG, "TLS Connection established...");
        break;
    }

    size_t written_bytes = 0;
    do
    {
        ret = esp_tls_conn_write(tls,
                                 request + written_bytes,
                                 strlen(request) - written_bytes);
        if (ret >= 0)
        {
            ESP_LOGI(TAG, "%d bytes written", ret);
            written_bytes += ret;
        }
        else if (ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            ESP_LOGE(TAG, "esp_tls_conn_write  returned 0x%x", ret);
            esp_tls_conn_delete(tls);
        }
    } while (written_bytes < strlen(request));

    ESP_LOGI(TAG, "Reading HTTP response...");

    // ---------------- End Example code from Espressif
    // ---------------- Below code is modified from the example
    bzero(response, BUF_SIZE); // Zero the output buffer
    while (1)
    {
        ret = esp_tls_conn_read(tls, response, BUF_SIZE);
        if (ret == MBEDTLS_ERR_SSL_WANT_WRITE || ret == MBEDTLS_ERR_SSL_WANT_READ)
            continue;

        if (ret < 0)
        {
            ESP_LOGE(TAG, "esp_tls_conn_read  returned -0x%x", -ret);
            break;
        }

        if (ret == 0)
        {
            ESP_LOGI(TAG, "connection closed");
            break;
        }

        ESP_LOGI(TAG, "%d bytes read", ret);
        break;
    }
    esp_tls_conn_delete(tls);
}

void get_http_code(char *data, char *code)
{
    snprintf(code, 4, "%c%c%c", data[9], data[10], data[11]);
}

void get_id(char *data, char *id)
{
    bzero(id, 30);
    for (int i = 0; i < BUF_SIZE; i++)
    {
        if (data[i] == '"' &&
            data[i + 1] == 'i' &&
            data[i + 2] == 'd' &&
            data[i + 3] == '"') // Searching for "id"
        {
            for (int j = i + 6; j < i + 30; j++)
            {
                id[0 + j - i - 6] = data[j];
            }
            break;
        }
    }
}

void get_token(char *data, char *token)
{
    bzero(token, 228);
    for (int i = 0; i < BUF_SIZE; i++)
    {
        if (data[i] == 't' &&
            data[i + 1] == 'o' &&
            data[i + 2] == 'k') // Searching for "tok" (token)
        {
            for (int j = i + 8; j < i + 227; j++)
            {
                token[0 + j - i - 8] = data[j];
            }
            break;
        }
    }
}

void get_session(char *data, session_info_t *session)
{
    for (int i = 0; i < BUF_SIZE; i++)
    {
        if (data[i] == '"' &&
            data[i + 1] == 'a' &&
            data[i + 2] == 'u' &&
            data[i + 3] == 't' &&
            data[i + 4] == 'h') // Searching for "auth (authorized)
        {
            if (data[i + 13] == 't') // If letter is t (true)
            {
                session->authorized = true;
            }
            else
            {
                session->authorized = false;
            }
        }

        if (data[i] == '"' &&
            data[i + 1] == 'i' &&
            data[i + 2] == 'd' &&
            data[i + 3] == 'e' &&
            data[i + 4] == 'n') // Searching for "iden (Identify)
        {
            if (data[i + 11] == 116)
            {
                session->identify = true;
            }
            else
            {
                session->identify = false;
            }
        }
    }
}

bool get_machine_id(char *data, char *machine_id)
{
    //  TODO Test!
    bzero(machine_id, 30);
    for (int i = 0; i < BUF_SIZE; i++)
    {
        if (data[i] == '"' &&
            data[i + 1] == 'm' &&
            data[i + 2] == 'a' &&
            data[i + 1] == 'c' &&
            data[i + 1] == 'h' &&
            data[i + 1] == 'i' &&
            data[i + 1] == 'n' &&
            data[i + 1] == 'e' &&
            data[i + 1] == '"') // Searching for "machine"
        {
            for (int j = i + 11; j < i + 31; j++)
            {
                machine_id[0 + j - i - 11] = data[j];
            }
            if (machine_id[0] == 'n' &&
                machine_id[1] == 'u' &&
                machine_id[2] == 'l' &&
                machine_id[3] == 'l')
            {
                bzero(machine_id, 30);
                return 1;
            }
            return 0;
        }
    }
    return 1;
}

void set_status(char *machine_id, char *token, bool status, char *response)
{
    char path[256];
    char data[128];

    snprintf(path,
             sizeof(path),
             "%s%s%s",
             WEB_URL,
             "/v1/machines/",
             machine_id);

    snprintf(data,
             sizeof(data),
             "%s%d%s",
             "{\'status\':",
             status,
             "}");
    patch_request(path, token, data, response);
}