/**
 * @file   ws2812b.c
 * @brief  ESP8266 driver for WS2812B
 * @author Ondřej Hruška, (c) 2016
 *
 * MIT License
 * 
 * Adapted by Kasper Laursen
 */

#include "ws2812.h"

/** Set one RGB LED color */
void ws2812_set(uint8_t gpio_num, uint32_t rgb)
{
    ws2812_seq_rgb(gpio_num, rgb);
}

/** Set many RGBs */
void ws2812_set_many(uint8_t gpio_num, uint32_t *rgbs, size_t count)
{
    for (size_t i = 0; i < count; i++)
    {
        uint32_t rgb = *rgbs++;
        ws2812_seq_rgb(gpio_num, rgb);
    }
}

/** Set one RGB to black (when used as indicator) */
void ws2812_off(uint8_t gpio_num)
{
    ws2812_set(gpio_num, 0x000000);
}