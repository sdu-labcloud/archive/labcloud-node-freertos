# Simple Node for SDU Labcloud

## Repository Structure

The structure of the repository follows the structure of the [project template](https://github.com/espressif/ESP8266_RTOS_SDK/tree/release/v3.1/examples/get-started/project_template) from the ESP8266 RTOS SDK. The main folder contains the program to run, while the components folder contains any custom components not found in the SDK.

Folders should be structured as follows:

- components
  - component&#46;mk
  - component1
    - component&#46;mk
    - component1.c
    - include
      - component1.h
  - component2
    - component&#46;mk
    - component2.c
    - include
      - component2.h

Note that the component&#46;mk file inside the component folders is simply an empty makefile.\
The top makefile inside the components folder should contain the following:

```makefile
COMPONENT_SRCDIRS := component1, component2
```

Assuming you have two component folders.

## Building the project

This project utilizes Gitlab's CI/CD pipeline to build the project. To build the project manually, follow the instructions below.

To build the project, the ESP8266 RTOS SDK V3.1 is used. To avoid installing the toolchain locally, a Docker image can be used, found [here](https://hub.docker.com/r/clapfire/espbuilder). Use tag 3.1.\
Create a container using the image, and clone this repo somewhere within the container. Navigate to the repository and execute:

```shell
make menuconfig
```

This will allow you to configure the project. Once configured, this will not need to be done again.\
After the project is configured correctly, to build the whole project, execute:

```shell
make
```

To only rebuild the app, and not the bootloader, execute:

```shell
make app
```

To see other options, execute:

```shell
make help
```

## Flashing the Node

To flash the node with the new firmware, use python and esptool. Install python from their [website](python.org) and use pip to install esptool:

```shell
python pip install esptool
```

Download the firmware files from Gitlab and unzip them. Navigate to the directory you unzipped the files to and run:

```shell
python -m esptool --chip esp8266 --baud 115200 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 40m --flash_size 1MB 0x0000 bootloader/bootloader.bin 0x10000 simple-node.bin 0x8000 partitions_singleapp.bin
```
