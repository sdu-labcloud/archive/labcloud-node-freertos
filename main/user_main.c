#include <stdio.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"

#include "esp_system.h"
#include "esp_event_loop.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_wifi.h"

#include "driver/hw_timer.h"
#include "driver/uart.h"
#include "esp8266/uart_register.h"

#include "rom/ets_sys.h"
#include "nvs_flash.h"

#include "requests.h"
#include "ws2812.h"
#include "sonoff_gpio.h"

//menu configured options
#define ESP_WIFI_SSID CONFIG_ESP_WIFI_SSID
#define ESP_WIFI_PASS CONFIG_ESP_WIFI_PASSWORD
#define MAX_STA_CONN CONFIG_MAX_STA_CONN
#define ESP_WIFI_PORT CONFIG_PORT

#define SECOND (1000 / portTICK_PERIOD_MS)
#define vTaskDelayMs(ms) vTaskDelay((ms) / portTICK_PERIOD_MS)

static const char *TAG = "Simple node";

// Queues
QueueHandle_t get_request_queue;
QueueHandle_t post_request_path_queue;
QueueHandle_t post_request_data_queue;
QueueHandle_t response_queue;
QueueHandle_t uid_queue;
QueueHandle_t indicator_led_queue;

// From ESP8266_RTOS_SDK simple wifi example:
/* FreeRTOS event group to signal when we are connected*/
EventGroupHandle_t wifi_event_group;
EventGroupHandle_t neopixel_event_group;

esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch (event->event_id)
    {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_START");
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "got ip:%s",
                 ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
        xEventGroupSetBits(wifi_event_group, BIT0);
        break;
    case SYSTEM_EVENT_AP_STACONNECTED:
        ESP_LOGI(TAG, "station:" MACSTR " join, AID=%d",
                 MAC2STR(event->event_info.sta_connected.mac),
                 event->event_info.sta_connected.aid);
        break;
    case SYSTEM_EVENT_AP_STADISCONNECTED:
        ESP_LOGI(TAG, "station:" MACSTR "leave, AID=%d",
                 MAC2STR(event->event_info.sta_disconnected.mac),
                 event->event_info.sta_disconnected.aid);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        esp_wifi_connect();
        xEventGroupClearBits(wifi_event_group, BIT0);
        break;
    default:
        break;
    }
    return ESP_OK;
}

void timer_callback(void *arg)
{
    xEventGroupSetBits(neopixel_event_group, BIT0);
}

esp_err_t nvt_init()
{
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    return ESP_OK;
}
esp_err_t wifi_ap_init()
{
    wifi_event_group = xEventGroupCreate();

    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    wifi_config_t wifi_config = {
        .ap = {
            .ssid = ESP_WIFI_SSID,
            .ssid_len = strlen(ESP_WIFI_SSID),
            .password = ESP_WIFI_PASS,
            .authmode = WIFI_AUTH_WPA_WPA2_PSK,
            .max_connection = MAX_STA_CONN},
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_LOGI(TAG, "wifi_init_softap finished.SSID:%s password:%s",
             ESP_WIFI_SSID, ESP_WIFI_PASS);

    return ESP_OK;
}

esp_err_t wifi_sta_init()
{
    wifi_event_group = xEventGroupCreate();

    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = ESP_WIFI_SSID,
            .password = ESP_WIFI_PASS,
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_LOGI(TAG, "wifi_init_sta finished.");

    return ESP_OK;
}

esp_err_t timer_init()
{
    neopixel_event_group = xEventGroupCreate();
    hw_timer_init(timer_callback, NULL);
    hw_timer_set_clkdiv(TIMER_CLKDIV_1);

    return ESP_OK;
}

esp_err_t uart_init()
{
    uart_config_t uart_config;

    uart_config.baud_rate = 9600;
    uart_config.data_bits = UART_DATA_8_BITS;
    uart_config.parity = UART_PARITY_DISABLE;
    uart_config.stop_bits = UART_STOP_BITS_1;
    uart_config.flow_ctrl = UART_HW_FLOWCTRL_DISABLE;
    uart_config.rx_flow_ctrl_thresh = 0;

    ESP_ERROR_CHECK(uart_param_config(UART_NUM_0, &uart_config));
    ESP_ERROR_CHECK(uart_driver_install(UART_NUM_0, 256, 256, 16, NULL));

    return ESP_OK;
}

esp_err_t init()
{
    get_request_queue = xQueueCreate(5, 128);
    post_request_path_queue = xQueueCreate(5, 128);
    post_request_data_queue = xQueueCreate(5, 128);
    response_queue = xQueueCreate(2, BUF_SIZE);
    uid_queue = xQueueCreate(10, sizeof(uint32_t));
    indicator_led_queue = xQueueCreate(5, sizeof(uint32_t));

    ESP_ERROR_CHECK(uart_init());
    ESP_ERROR_CHECK(sonoff_gpio_init());
    ESP_ERROR_CHECK(nvt_init());
    ESP_ERROR_CHECK(wifi_sta_init());
    ESP_ERROR_CHECK(timer_init());

    rtc_clk_cpu_freq_set(RTC_CPU_FREQ_160M);
    return ESP_OK;
}

static void uart_task(void *param)
{
    static const char *TAG = "UART Task";

    uint8_t data_buf[5];
    uint32_t uid = 0;

    while (1)
    {
        bzero(data_buf, 5);
        uart_read_bytes(UART_NUM_0, data_buf, 5, SECOND * 60);
        // calculating unsigned int from hex
        uid = data_buf[1] |
              data_buf[2] << 8 |
              data_buf[3] << 16 |
              data_buf[4] << 24;
        xQueueSendToBack(uid_queue, (void *)&uid, SECOND * 60);

        vTaskDelay(SECOND);
    }
}

static void uid_task(void *param)
{
    static const char *TAG = "UID task";

    uint32_t uid = 0;

    while (1)
    {
        xQueueReceive(uid_queue, &uid, SECOND * 60);
        ESP_LOGI(TAG, "UID: %u", uid);
    }
}

static void neopixel_task(void *param)
{
    static const char *TAG = "Neopixel Task";
    uint32_t color = 0;

    while (1)
    {
        xQueueReceive(indicator_led_queue, &color, SECOND * 60);
        ws2812_set(INDICATOR_LED_PIN, color);
    }
}

static void get_request_task(void *param)
{
    static const char *TAG = "Get Request Task";

    char path[128];
    char response[BUF_SIZE];

    // Make sure device is connected to wifi
    ESP_LOGI(TAG, "Waiting for wifi...");
    xEventGroupWaitBits(wifi_event_group, BIT0,
                        false, true, portMAX_DELAY);
    ESP_LOGI(TAG, "Connected!");

    while (1)
    {
        if (xQueueReceive(get_request_queue, path, SECOND * 60) == pdFALSE)
        {
            continue;
        }
        get_request(path, response);
        xQueueSendToBack(response_queue, response, SECOND * 60);
    }
}

static void post_request_task(void *param)
{
    static const char *TAG = "Post Request Task";

    char path[128];
    char response[BUF_SIZE];
    char data[128];

    // Make sure device is connected to wifi
    ESP_LOGI(TAG, "Waiting for wifi...");
    xEventGroupWaitBits(wifi_event_group, BIT0,
                        false, true, portMAX_DELAY);
    ESP_LOGI(TAG, "Connected!");

    while (1)
    {
        if (xQueueReceive(post_request_path_queue, path, SECOND * 60) == pdFALSE)
        {
            continue;
        }

        xQueueReceive(post_request_data_queue, data, SECOND * 60);
        post_request(path, data, response);
        xQueueSendToBack(response_queue, response, SECOND * 60);
    }
}

void app_main(void)
{
    static const char *TAG = "Main Thread";
    ESP_LOGI(TAG, "SDK version:%s\n", esp_get_idf_version());

    uint8_t relay_level = 0;
    char response[BUF_SIZE];
    char path[128];
    char response_code[4];
    char node_id[30];
    char session_id[30];
    char machine_id[30];
    char token[228];
    uint32_t color = 0;

    session_info_t session;

    ESP_ERROR_CHECK(init());

    xTaskCreate(uart_task, "uart_task", 1024, NULL, 1, NULL);
    xTaskCreate(uid_task, "uid_task", 1024, NULL, 1, NULL);
    xTaskCreate(neopixel_task, "neopixel_task", 1024, NULL, 3, NULL);
    xTaskCreate(get_request_task, "get_request_task", 8192, NULL, 2, NULL);
    xTaskCreate(post_request_task, "post_request_task", 8192, NULL, 2, NULL);

    SONOFF_POWER_LED_SET_LEVEL(true);

    uint8_t mac[6];
    esp_efuse_mac_get_default(mac);

    char mac_string[13];
    snprintf(mac_string, sizeof(mac_string), "%x%x%x%x%x%x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

    uint8_t serial[4];
    for (int i = 0; i < 4; i++)
    {
        serial[i] = mac[i] * 5;
    }

    char serial_string[9];
    snprintf(serial_string, sizeof(serial_string), "%x%x%x%x", serial[0], serial[1], serial[2], serial[3]);

    char data_string[91];
    snprintf(data_string,
             sizeof(data_string),
             "{\"serial_number\":\"%s\",\"mac_address\":\"%s\",\"operating_system\":\"micropython\"}",
             serial_string,
             mac_string);

    post_request_t create_node;
    strcpy(create_node.path, "/v1/nodes");
    strcpy(create_node.data, data_string);

    while (1)
    {
        xQueueSendToBack(post_request_path_queue, create_node.path, SECOND * 60);
        xQueueSendToBack(post_request_data_queue, create_node.data, SECOND * 60);
        xQueueReceive(response_queue, response, SECOND * 60);
        ESP_LOGI(TAG, response);
        get_http_code(response, response_code);
        ESP_LOGI(TAG, "Response code: %s", response_code);
        get_id(response, node_id);
        ESP_LOGI(TAG, "Node ID: %s", node_id);

        post_request_t create_session;

        snprintf(create_session.path, sizeof(create_session.path), "/v1/nodes/%s/sessions", node_id);
        snprintf(create_session.data, sizeof(create_session.data), " ");

        xQueueSendToBack(post_request_path_queue, create_session.path, SECOND * 60);
        xQueueSendToBack(post_request_data_queue, create_session.data, SECOND * 60);
        xQueueReceive(response_queue, response, SECOND * 60);
        ESP_LOGI(TAG, response);
        get_http_code(response, response_code);
        ESP_LOGI(TAG, "Response code: %s", response_code);
        get_id(response, session_id);
        ESP_LOGI(TAG, "Session ID: %s", session_id);
        get_token(response, token);
        ESP_LOGI(TAG, "Token: %s", token);

        snprintf(path, sizeof(path), "/v1/nodes/%s/sessions/%s", node_id, session_id);

        do
        {
            xQueueSendToBack(get_request_queue, path, SECOND * 60);
            xQueueReceive(response_queue, response, SECOND * 60);
            ESP_LOGI(TAG, response);
            get_session(response, &session);
            ESP_LOGI(TAG, "Authorized: %d", session.authorized);
            ESP_LOGI(TAG, "Identify: %d", session.identify);
            if (session.identify)
            {
                color = GREEN;
                xQueueSendToBack(indicator_led_queue, &color, 60 * SECOND); // Green
            }
            else
            {
                color = OFF;
                xQueueSendToBack(indicator_led_queue, &color, 60 * SECOND); // Off
            }
        } while (!session.authorized);

        // Waiting for machine to be assigned
        do
        {
            snprintf(path, sizeof(path), "/v1/nodes/%s", node_id);
            xQueueSendToBack(get_request_queue, path, SECOND * 60);
            xQueueReceive(response_queue, response, SECOND * 60);
            ESP_LOGI(TAG, "Machine ID: %s", machine_id);
        } while (get_machine_id(response, &machine_id));

        ESP_LOGI(TAG, "\nFree heap: %u\nMinimum heap: %u", esp_get_free_heap_size(), esp_get_minimum_free_heap_size());

        while (1)
        {
            if (relay_level == 1)
            {
                SONOFF_RELAY_SET_LEVEL(false);
                relay_level = 0;
                ESP_LOGI(TAG, "OFF");
            }
            else
            {
                SONOFF_RELAY_SET_LEVEL(true);
                relay_level = 1;
                ESP_LOGI(TAG, "ON");
            }
            vTaskDelay(5 * SECOND);
        }
    };
}